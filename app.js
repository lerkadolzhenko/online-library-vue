const express = require("express")
const mongoose = require("mongoose")
const config = require("config")
let cors = require("cors")
const bodyParser = require("body-parser");

const corsOptions = {
  origin: "http://localhost:8080",
}

const app = express()
app.use(express.json({ extended: true }))
app.use(cors(corsOptions))
app.use(bodyParser.json())

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
app.use("/api/auth", require("./routes/auth_routes"))
app.use("/api/books", require("./routes/books_routes"))

const PORT = 7000

async function start() {
  try {
    await mongoose.connect(config.get("mongoUri"), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    })
    app.listen(PORT, () => console.log(`app started on ${PORT}`))
  } catch (e) {
    console.log("Server error", e.message)
    process.exit(1)
  }
}
start()
