const { Schema, model, Types } = require("mongoose")

const schema = new Schema({
  title: { type: String, required: true, unique: true },
  description: { type: String, required: true },
  pageCount: { type: Number, required: true },
  bookText: { type: String, required: true },
  author:{  type: String, required: true },
  publishDate: { type: String, required: true },
  photo: { type: String, required: true }
})

module.exports = model("Book", schema)
