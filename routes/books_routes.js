const { Router } = require("express")
const router = Router()
const config = require("config")
const Book = require("../models/Book")
const { check, validationResult } = require("express-validator")

//api/books/addbook
router.post(
  "/addbook",
  [
    check("title", "title must be min 6 symbols").isLength({ min: 1 }),
    check("description", "description must be min 6 symbols").isLength({
      min: 1,
    }),
    check("pageCount", "description must be min 2 symbols").isLength({
      min: 2,
    }),
    check("bookText", "bookText must be min 100 symbols").isLength({
      min: 1,
    }),
    check("author", "bookText must be min 5 symbols").isLength({ min: 1 }),
    check("photo", "bookText must be min 10 symbols").isLength({ min: 1 }),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: "book data is not correct",
        })
      }
      const {
        title,
        description,
        pageCount,
        bookText,
        author,
        publishDate,
        photo,
      } = req.body

      const candidateBook = await Book.findOne({ title: title })
      if (candidateBook) {
        return res.status(400).json({ message: "This book already exist" })
      }

      const book = new Book({
        title: title,
        description: description,
        pageCount: pageCount,
        bookText: bookText,
        author: author,
        publishDate: publishDate,
        photo: photo,
      })
      await book.save()
      res.status(201).json({ message: "book was created" })
    } catch (e) {
      res
        .status(500)
        .json({ message: "please try again , something went wrong. addbook" })
    }
  }
)

router.get("/", async (req, res) => {
  Book.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

router.get("/:id", async (req, res) => {
  try {
    const books = await Book.findById(req.params.id)
    res.json(books)
  } catch (e) {
    res.status(500).json({ message: "smth went wrong boos get" })
  }
})

module.exports = router
