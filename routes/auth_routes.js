const { Router } = require("express")
const router = Router()
const jwt = require("jsonwebtoken")
const config = require("config")
const bcrypt = require("bcryptjs")
const User = require("../models/User")
const { check, validationResult } = require("express-validator")

//api/auth/register
router.post(
  "/register",
  [
    check("email", "email is not correct").isEmail(),
    check("password", "password must be min 6 symbols").isLength({ min: 6 }),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: "your register data is not correct",
        })
      }
      const { email, password } = req.body
      const candidate = await User.findOne({ email: email })
      if (candidate) {
        return res.status(400).json({ message: "This user already exist" })
      }
      const hashedPassword = await bcrypt.hash(password, 12)
      const user = new User({ email: email, password: hashedPassword })
      await user.save()
      res.status(201).json({ message: "user was created" })
    } catch (e) {
      res
        .status(500)
        .json({ message: "please try again , something went wrong. register" })
    }
  }
)

//api/auth/login
router.post(
  "/login",
  [
    check("email", "enter correct email").normalizeEmail().isEmail(),
    check("password", "enter password").exists(),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: "your login data is not correct",
        })
      }

      const { email, password } = req.body

      const user = await User.findOne({ email })
      if (!user) {
        return res.status(400).json({ message: "user not found" })
      }

      const isMatch = await bcrypt.compare(password, user.password)
      if (!isMatch) {
        return res.status(400).json({ message: "password is not correct " })
      }

      const token = jwt.sign({ userId: user.id }, config.get("jwtSecret"), {
        expiresIn: "1h",
      })
      res.json({ token, userId: user.id, message: "you are loggined"  })
    } catch (e) {
      res
        .status(500)
        .json({ message: "please try again , something went wrong. register" })
    }
  }
)

module.exports = router
